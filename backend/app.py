'''Flask App For PyGuy WebSite'''

from flask import Flask, jsonify, render_template, request
from flask_pymongo import PyMongo

# App Definitions & Configs
app = Flask(__name__, root_path='../frontend')
app.config["MONGO_DBNAME"] = 'pysite'
app.config["MONGO_URI"] = "mongodb://localhost:27017/pysite"
mongo = PyMongo(app)

# This will be the index route ....


"""
Defining all API routes. 
"""


@app.route("/")
def index():
    return render_template("index.html")


@app.route('/posts', methods=['GET'])
def articles():
    articles = mongo.db.articles

    output = []

    for q in articles.find():
        output.append(
            {'title': q['title'], 'description': q['description'], 'tags': q['tags']})
    return jsonify({'result': output})


@app.route("/add_articles", methods=["POST"])
def add_articles():
    article = mongo.db.articles

    title = request.json['title']
    description = request.json['description']
    tags = request.json['tags']
    article_id = article.insert(
        {'title': title, 'description': description, 'tags': tags})

    new_article = article.find_one({'_id': article_id})
    output = {'title': new_article['title'],
              'description': new_article['description'], 'tags': new_article['tags']}
    return jsonify({'result': output})


if __name__ == "__main__":
    app.run(debug=True)
